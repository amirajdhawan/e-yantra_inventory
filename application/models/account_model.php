<?php
class Account_model extends CI_Model {
	
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public function authenticate(){
		
		$this->load->helper('security');

		$ret_auth_data = null;
		$username = $this->input->post('username');
		//$hash = do_hash($this->input->post('password'), 'md5');
		
		$data = array(
			'USERNAME' => $username
		);

		$result = $this->db->get_where('LOGIN',$data);
		$num_row = $result->num_rows();
		
		if($num_row === 1){
			$row_data = $result->row_array();
			$salt = $row_data['SALT'];
			$salt = trim($salt);
			//echo $salt;
			$hash = do_hash(($this->input->post('password').$salt), 'md5');
			//echo "<br/>".$hash;

			$log_data = array(
				'USERNAME' 	=>	$username,
				'PWD'		=>	$hash,
				'SALT'		=>	$salt
			);
			$log_result = $this->db->get_where('LOGIN',$log_data);
			
			if($log_result->num_rows() === 1){
				$log_row_data = $log_result->row_array();
				$ret_auth_data = array(
					'user' 	=>	$username,
					'role' 	=> 	$log_row_data['ROLE'],
					'auth' 	=> 	1
				);	
			}
			else{
				$ret_auth_data = array(
					'auth' => 0
				);	
			}
		}
		else{
			$ret_auth_data = array(
				'auth' => 0);
		}
		return $ret_auth_data;
	}
}