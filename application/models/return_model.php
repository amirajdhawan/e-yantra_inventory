<?php
class Return_model extends CI_Model {
	
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public function get_users_return(){
		$uname = $this->input->post('uname');
		$return_data = array();
		$user_ids = array(NULL);
		$user = array();

		if($uname != NULL){
			$user_dets = $this->db->select()->from('USER')->like(array('USER_NAME' => $uname))->get();
			foreach($user_dets->result() as $row){
				array_push($user_ids, $row->USER_ID);
				$user[$row->USER_ID] = $row->USER_NAME;
			}	
		}
				
		$issues = $this->db->select()->from('ISSUE')->where('RETVAR IS NULL',NULL)->where_in('USER_ID', $user_ids)->get();
		
		foreach($issues->result() as $row){
		
			$prod = $this->db->select('PROD_NAME')->from('PRODUCT')->where('PROD_ID', $row->PROD_ID)->get();
			if($prod->num_rows > 0){
				$prod_row = $prod->row();
				$prod_name = $prod_row->PROD_NAME;

				$temp = array(
					'ISSUE_ID'	=>	$row->ISSUE_ID,
					'ISSUE_DT'	=>	date("d/m/Y",strtotime($row->ISSUE_DT)),
					'QTY'		=>	$row->QTY,
					'USER_ID'	=>	$row->USER_ID,
					'USER_NAME'	=>	$user[$row->USER_ID],
					'PROD_ID'	=>	$row->PROD_ID,
					'PROD_NAME'	=>	$prod_name	
				);

				array_push($return_data,$temp);
			}
			//print_r($row);
		}
		return json_encode($return_data);
	}

	public function get_prods_return(){
		$pname = $this->input->post('pname');
		//$pname = 'ab';
		$return_data = array();
		$prod_ids = array(NULL);
		$products = array();

		if($pname != NULL){
			$prod_dets = $this->db->select()->from('PRODUCT')->like(array('PROD_NAME' => $pname))->get();
			foreach($prod_dets->result() as $row){
				array_push($prod_ids, $row->PROD_ID);
				$products[$row->PROD_ID] = $row->PROD_NAME;
			}
		}
		
		$issues = $this->db->select()->from('ISSUE')->where('RETVAR IS NULL',NULL)->where_in('PROD_ID', $prod_ids)->get();

		foreach($issues->result() as $row){
			$user = $this->db->select('USER_NAME')->from('USER')->where('USER_ID',$row->USER_ID)->get();
			if($user->num_rows > 0){
				$user_row = $user->row();
				$user_name = $user_row->USER_NAME;

				$temp = array(
					'ISSUE_ID'	=>	$row->ISSUE_ID,
					'USER_ID'	=>	$row->USER_ID,
					'USER_NAME'	=>	$user_name,
					'PROD_ID'	=>	$row->PROD_ID,
					'PROD_NAME'	=>	$products[$row->PROD_ID]
				);
				array_push($return_data,$temp);
			}
		}
		return json_encode($return_data);
	}

	public function get_issue_details(){
		$prod_id = $this->input->post('prod_id');
		$u_id = $this->input->post('user_id');

		$where_clause = array(
			'PROD_ID'	=>	$prod_id,
			'USER_ID'	=>	$u_id,
			'RETVAR'	=>	NULL	
		);

		$result = $this->db->select()->from('ISSUE')->where($where_clause)->get();
		$prod_dets = $this->db->select()->from('PRODUCT')->where('PROD_ID',$prod_id)->get();
		$user_dets = $this->db->select()->from('USER')->where('USER_ID',$u_id)->get();

		//$result_json = json_encode($result->first_row());
		if($result->num_rows() > 0 && $prod_dets->num_rows() > 0 && $user_dets->num_rows() > 0){
			$user_row = $user_dets->first_row();
			$row = $result->first_row();
			$prod_detail1 = $prod_dets->first_row();
			$prod_detail = array($prod_detail1);
			//$row['USER_NAME'] = $user_row->USER_NAME;
			//print_r($row);
			$row->USER_NAME = $user_row->USER_NAME;
			
			unset($prod_detail['PROD_ID']);
			$return_data['ISSUE_DETAILS'] = array($row);
			$return_data['PROD_DETAIL'] = $prod_detail;
			// $return_data = array_merge(, $prod_detail);
			//print_r($return_data);
			//$return_data = array($return_data);

			$return_json = json_encode($return_data);
			return $return_json;
		}
	}

	public function return_hardware(){
		$ret_quantity = $this->input->post('return_quan');
		$issue_id = $this->input->post('issue_id');
		
		$result = $this->db->select()->from('ISSUE')->where('ISSUE_ID',$issue_id)->get();
		$row = $result->first_row();
		//echo "in";
		$issued_quan = $row->QTY;
		
		if($ret_quantity > $issued_quan){
			return FALSE;
		}
		else{
			if($ret_quantity == $issued_quan){
				//print_r($ret_quantity);
				$this->db->where('ISSUE_ID',$issue_id)->set('RET_DT','NOW()',FALSE)->update('ISSUE',array('RETVAR' => 1));
			}
			else{
				$this->db->where('ISSUE_ID',$issue_id)->set('RET_DT','NOW()',FALSE)->update('ISSUE',array('RETVAR' => 1, 'QTY' => $ret_quantity));
				$insert_data = array(
					'PROD_ID'	=>	$row->PROD_ID,
					'USER_ID'	=>	$row->USER_ID,
					'ISSUE_DT'	=>	$row->ISSUE_DT,
					'RET_DT'	=>	NULL,
					'RETVAR'	=>	NULL,
					'QTY'		=>	$issued_quan - $ret_quantity
				);

				$this->db->insert('ISSUE',$insert_data);
			}

			$prod_res = $this->db->select()->from('PRODUCT')->where('PROD_ID',$row->PROD_ID)->get();
			$prod_row = $prod_res->first_row();
			$avail_quan	= $prod_row->AVAIL_QTY;
			$this->db->where('PROD_ID',$row->PROD_ID)->update('PRODUCT',array('AVAIL_QTY' => ($avail_quan + $ret_quantity)));

			return TRUE;
		}
	}

	public function add_breakage(){
		
	}
}