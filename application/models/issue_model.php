<?php
class Issue_model extends CI_Model {
	
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public function get_user_names(){
		$username = $this->input->post('name');
		//$username = 'amiraj';
		$result = $this->db->select()->from('USER')->like('USER_NAME',$username)->get();

		$data = array();
		
		if($result->num_rows() > 0){
			foreach( $result->result() as $row ){
				array_push($data, $row);
			}
		}
		$res = json_encode($data);

		return $res;
	}

	public function get_user_details(){
		$user_id = $this->input->post('user_id');
		//$user_id = 1;
		$result = $this->db->select()->from('USER')->where('USER_ID',$user_id)->get();

		$result_json = json_encode($result->first_row());

		return $result_json;
	}
	
	public function get_prod_names(){
		$prod_name = $this->input->post('name');
		//$username = 'amiraj';
		$result = $this->db->select()->from('PRODUCT')->like('PROD_NAME',$prod_name)->get();

		$data = array();
		
		if($result->num_rows() > 0){
			foreach( $result->result() as $row ){
				array_push($data, $row);
			}
		}
		$res = json_encode($data);
		//print_r($res);

		return $res;
	}	

	public function get_prod_details(){
		$prod_id = $this->input->post('prod_id');
		//$prod_id = 2;
		$result = $this->db->select()->from('PRODUCT')->where('PROD_ID',$prod_id)->get();

		$result_json = json_encode($result->first_row());

		return $result_json;
	}

	public function check_user(){
		$username = $this->input->post('uname');
		$address = $this->input->post('uaddress');
		$email = $this->input->post('uemail');
		$phone_num = $this->input->post('uphn');
		$email = $this->input->post('uemail');

		$where_clause = array(
			'USER_NAME' => $username,
			'ADDRESS' 	=> $address,
			'EMAIL'		=> $email,
			'PHONE_NUM' => $phone_num
		);

		$result = $this->db->select()->from('USER')->where($where_clause)->get();

		if($result->num_rows() === 1){
			$user_id = $result->row()->USER_ID;
			return $user_id;
		}

		else{	
			//$this->load->model('issue_model');
			$user_id = $this->add_user($where_clause);

			if(!($user_id == NULL))
				return $user_id;
			else
				return NULL;
		}
	}

	public function add_user($data){
		
		if($this->db->insert('USER',$data)){
			return $this->db->insert_id();
		}
		else
			return NULL;
	}

	public function issue_prod(){
		//$this->load->model('issue_model');
		$user_id = $this->check_user();
		$prod_id = $this->get_prod_id();
		$row = $this->get_prod_avail($prod_id);
		$avail_qty = $row->AVAIL_QTY;
		$quantity  = $this->input->post('quantity');

		$return_array = array(
			'mode'		=>	FALSE,
			'issue_id'	=>	null
		);

		if(($quantity <= $avail_qty) && ($quantity > 0)){
			//print_r($quantity);
			//print_r($avail_qty);
			$insert_data = array(
				'USER_ID'	=>	$user_id,
				'PROD_ID'	=>	$prod_id,
				'QTY'		=>	$quantity
			);
			$this->db->set('ISSUE_DT','NOW()',FALSE);

			if($this->db->insert('ISSUE',$insert_data)){
				$return_array['mode'] = 1;
				$return_array['issue_id'] = $this->db->insert_id();
				$new_avail = $avail_qty - $quantity;
				$this->db->update('PRODUCT', array('AVAIL_QTY' => $new_avail), array('PROD_ID' => $prod_id));
				return $return_array;
			}
			else{
				$return_array['mode'] = 2;
				$return_array['issue_id'] = NULL;
				return $return_array;
			}
		}
		else{
			if($quantity < 1){
				$return_array['mode'] = 4;
				$return_array['issue_id'] = NULL;
				return $return_array;
			}
			else{
				$return_array['mode'] = 3;
				$return_array['issue_id'] = NULL;
				return $return_array;
			}
		}
	}

	public function get_prod_avail($prod_id){
		$result = $this->db->select()->from('PRODUCT')->where(array('PROD_ID' => $prod_id))->get();
		$row = $result->first_row();
		return $row;
	}

	/*public function get_prod_tot($prod_id){
		$result = $this->db->select()->from('PRODUCT')->where(array('PROD_ID' => $prod_id))->get();
		$row = $result->row();
		return $row->TOT_QTY;
	}*/

	public function get_prod_id(){
		$prod_name = $this->input->post('prod_name');
		$prod_manu = $this->input->post('prod_manu');
		$prod_desc = $this->input->post('prod_desc');

		$where_clause = array(
			'PROD_NAME'		=> 	$prod_name,
			'MANUFACT'		=> 	$prod_manu,
			'DESCRIPTION'	=> 	$prod_desc
		);

		$result = $this->db->select()->from('PRODUCT')->where($where_clause)->get();
		
		if($result->num_rows() == 1){
			return $result->row()->PROD_ID;
		}
		else
			return NULL;
	}

	public function add_breakage(){
	
		//$this->load->model('issue_model');
		$user_id = $this->check_user();
		$prod_id = $this->get_prod_id();
		$row = $this->get_prod_avail($prod_id);
		$avail_qty = $row->AVAIL_QTY;
		$tot_qty = $row->TOT_QTY;
		$breakage = $row->BREAKAGE;
		$quantity  = $this->input->post('quantity');
		$type = $this->input->post('type');
		$type_int = NULL;
		$comments = $this->input->post('comments');

		switch($type){
			case 'broken':
				$type_int = 1;
				break;
			case 'lost':
				$type_int = 2;
				break;
			case 'gifted':
				$type_int = 3;
				break;
			case 'writeoff':
				$type_int = 4;
				break;
		}

		$return_array = array(
			'mode'		=>	FALSE,
			'break_id'	=>	NULL
		);

		if(($quantity <= $avail_qty) && ($quantity > 0)){

			$insert_data = array(
				'USER_ID'	=>	$user_id,
				'PROD_ID'	=>	$prod_id,
				'QTY'		=>	$quantity,
				'TYPE'		=>	$type_int,
				'COMMENTS'	=>	$comments
			);
			$this->db->set('ENTRY_DATE','NOW()',FALSE);

			if($this->db->insert('BREAKAGE', $insert_data)){
				$return_array['mode'] = 1;
				$return_array['break_id'] = $this->db->insert_id();
				$new_avail = $avail_qty - $quantity;
				$new_tot = $tot_qty - $quantity;
				$new_break = $breakage + $quantity;
				$this->db->update('PRODUCT', array('AVAIL_QTY' => $new_avail, 'TOT_QTY' => $new_tot, 'BREAKAGE' => $new_break), array('PROD_ID' => $prod_id));
				return $return_array;
			}
			else{
				$return_array['mode'] = 2;
				$return_array['break_id'] = NULL;
				return $return_array;
			}
		}
		else{
			if($quantity < 1){
				$return_array['mode'] = 4;
				$return_array['break_id'] = NULL;
				return $return_array;
			}
			else{
				$return_array['mode'] = 3;
				$return_array['break_id'] = NULL;
				return $return_array;
			}
		}	
	}
	
}