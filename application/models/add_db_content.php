<?php
class Add_db_content extends CI_Model {
	
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public function get_avail_issues(){
		$avail = $this->db->select()->from('PRODUCT')->get();
		$data = array();

		foreach($avail->result() as $row){
			if($row->AVAIL_QTY <= ($row->TOT_QTY * 0.10) || ($row->AVAIL_QTY <= 4 && $row->TOT_QTY >= 10) || ($row->AVAIL_QTY == 1 && $row->TOT_QTY <= 3)){
				$row->TEMP = 1;
				array_push($data,$row);
			}
			else if($row->AVAIL_QTY <= ($row->TOT_QTY * 0.20) || ($row->AVAIL_QTY <= 8 && $row->TOT_QTY >=20)  || ($row->AVAIL_QTY == 2 && $row->TOT_QTY <= 5)){
				$row->TEMP = 2;
				array_push($data,$row);
			}
		}
		//print_r($data);
		return $data;

	}

	public function get_activity(){
		$issue_result = $this->db->select()->from('ISSUE')
						->join('PRODUCT','PRODUCT.PROD_ID = ISSUE.PROD_ID')
						->join('USER','USER.USER_ID = ISSUE.USER_ID')
						->where('ISSUE_DT <= adddate(curdate(), interval 1 day) AND ISSUE_DT > curdate()')->get();

		$return_result = $this->db->select()->from('ISSUE')
						->join('PRODUCT','PRODUCT.PROD_ID = ISSUE.PROD_ID')
						->join('USER','USER.USER_ID = ISSUE.USER_ID')
						->where('RET_DT <= adddate(curdate(), interval 1 day) AND RET_DT > curdate()')->get();

		if($issue_result->num_rows() == 0 && $return_result->num_rows() == 0){
			return NULL;
		}
		else{
			$compound_res = array();
			foreach($issue_result->result() as $row){
				$temp = $row;
				$temp->DATE = strtotime($row->ISSUE_DT);
				array_push($compound_res,$temp);
			}
				
			foreach ($return_result->result() as $row){
				$temp = $row;
				$temp->DATE = strtotime($row->RET_DT);
				array_push($compound_res,$temp);
			}

			$temp = array();
			
			foreach($compound_res as $key => $val){
				$temp[$key] = $val->DATE;
			}
			array_multisort($temp,SORT_DESC,$compound_res);
			//print_r($compound_res);
			return $compound_res;
		}
	}

	public function add_user_db(){
		$this->load->helper('security');
		$this->load->helper('string');
		$salt = random_string('alnum', 3);
		$salt = trim($salt);

		$username = $this->input->post('username');
		//$password = $this->input->post('password');
		$type = $this->input->post('type');
		$type_db = null;
		$hash = do_hash(($this->input->post('password').$salt), 'md5');
		//echo($salt);
		if($type == 'admin')
			$type_db = 1;								//1 for Administrator
		else
			$type_db = 2;								//2 for Viewer

		$add_user_data = array(
			'USERNAME' => $username,
			'PWD' 	   => $hash,
			'SALT'	   => $salt,
			'ROLE'	   => $type_db
			);

		if($this->db->insert('LOGIN', $add_user_data))
			return TRUE;
		else
			return FALSE;
		//return $data;
	}

	public function add_cat_db(){
		
		$cat_name = $this->input->post('cat_name');
		//$password = $this->input->post('password');
		$return_data = array(
			'cat_id' => null,
			'added'  => null
		);

		$add_cat_data = array(
			'CAT_NAME' => $cat_name,
		);

		$check_result = $this->db->get_where('CATEGORY',$add_cat_data);
		$rows_num = $check_result->num_rows();
		
		if($rows_num == 0){
			if($this->db->insert('CATEGORY', $add_cat_data)){
				$cat_id = $this->db->insert_id();
				$return_data['cat_id'] = $cat_id;
				$return_data['added'] = 1;
				return $return_data;
				//return TRUE;
			}
			else{
				$return_data['added'] = 0;
				return $return_data;
			}
		}
		else{
			$row_data = $check_result->row();
			$return_data['cat_id'] = $row_data->CAT_ID;
			$return_data['added'] = 2;
			return $return_data;
		}
		//return $data;
	}

	public function get_cat_data(){

		$result = $this->db->get("CATEGORY");
		$category_data = array();

		foreach($result->result_array() as $row){
			$temp = (string) strval($row['CAT_ID']);
			$category_data[$temp] = $row['CAT_NAME'];
		}
		
		return $category_data;
	}

	public function add_prod_db(){

		$prod_name = $this->input->post('prod_name');
		$prod_desc = $this->input->post('prod_desc');
		$prod_man = $this->input->post('prod_man');
		$prod_totq = $this->input->post('prod_totq');
		$prod_availq = $this->input->post('prod_availq');
		$cat_id = $this->input->post('cat_sel');

		if($prod_availq <= $prod_totq){
		//print_r($cat_id);
			$add_prod = array(
				'CAT_ID' => $cat_id,
				'PROD_NAME' => $prod_name,
				'DESCRIPTION' => $prod_desc,
				'MANUFACT' => $prod_man,
				'TOT_QTY' => $prod_totq,
				'AVAIL_QTY' => $prod_availq
				);

			$check_prod = array(
				'CAT_ID' => $cat_id,
				'PROD_NAME' => $prod_name,
				'DESCRIPTION' => $prod_desc,
				'MANUFACT' => $prod_man
				);

			$return_data = array();

			$check_result = $this->db->get_where('PRODUCT',$check_prod);
			$rows_num = $check_result->num_rows();

			if($rows_num == 0){
				if($this->db->insert('PRODUCT', $add_prod)){
					$prod_id = $this->db->insert_id();
					$return_data['prod_id'] = $prod_id;
					$return_data['added'] = 1;
					return $return_data;
					//return TRUE;
				}
				else{
					$return_data['added'] = 0;
					return $return_data;
				}
			}
			else{
				$return_data['added'] = 2;
				return $return_data;
			}
		}
		else{
			$return_data['added'] = 3;
			return $return_data;
		}
	}
}
