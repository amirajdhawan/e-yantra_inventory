<?php
class View_model extends CI_Model {
	
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public function get_cat_dets(){
		$cat_name = $this->input->post('cat_name');
		//$cat_name = 'ab';
		
		if($cat_name != NULL){
			$result = $this->db->select()->from('CATEGORY')->like('CAT_NAME',$cat_name)->get();
			$data = array();
			
			if($result->num_rows() > 0){
				foreach( $result->result() as $row ){
					array_push($data, $row);
				}
			}
			$res = json_encode($data);
			//print_r($res);

			return $res;
		}
		else
			return NULL;
	}

	public function cat_view(){
		$cat_id = $this->input->post('cat_id');
		//$cat_id = 20;

		if($cat_id != NULL){
			$page_no = $this->input->post('pg_no');
			$rows_per_page = 20;
			
			if($page_no == NULL || !is_numeric($page_no))
				$page_no = 1;
			
			$from_row = (($page_no - 1) * $rows_per_page);
			$to_row = $from_row + $rows_per_page - 1;
			
			$final_data = array();
			$data = array();
			$result = $this->db->select()->from('PRODUCT')->where('CAT_ID',$cat_id)->order_by('PROD_NAME','asc')->get();
			
			if($result->num_rows() > 0){
				$row_tot = $result->num_rows();
				$final_data['rows'] = $row_tot;
				foreach($result->result() as $row){
					array_push($data,$row);
				}
			}

			$list_data = array();
			$count = 0;

			foreach($data as $key => $val){
				if($count < $from_row)
					$count++;
				else{
					if($count > $to_row)
						$count++;
					else{
						array_push($list_data, $val);
						$count++;
					}
				}
			}
			$final_data['table'] = $list_data;
			$res = json_encode($final_data);
			return $res;
		}
		else
			return NULL;
	}

	public function get_prod_dets(){
		$prod_name = $this->input->post('prod_name');

		//$prod_name = 'ab';
		if($prod_name != NULL){
			$result = $this->db->select()->from('PRODUCT')->like('PROD_NAME',$prod_name)->get();
			$data = array();
			
			if($result->num_rows() > 0){
				foreach( $result->result() as $row ){
					array_push($data, $row);
				}
			}
			$res = json_encode($data);
			//print_r($res);

			return $res;
		}
		else
			return NULL;
	}

	public function prod_view(){
		$prod_id = $this->input->post('prod_id');
		
		if($prod_id != NULL){
			$from_dt = $this->input->post('from_dt');
			$to_dt = $this->input->post('to_dt');
			$from_dt = str_replace('/','-',$from_dt);
			$to_dt = str_replace('/','-',$to_dt);
			$from_dt = strtotime($from_dt);
			$to_dt = strtotime($to_dt) + 86400;
			$from_dt_str = date('Y-m-d',$from_dt);
			$to_dt_str = date('Y-m-d',$to_dt);
			$page_no = $this->input->post('pg_no');
			$rows_per_page = 20;
			
			if($page_no == NULL || !is_numeric($page_no))
				$page_no = 1;
			
			//$to_row = $page_no * 20;
			$from_row = (($page_no - 1) * $rows_per_page);
			$to_row = $from_row + $rows_per_page - 1;
			
			$final_data = array();
			$data = array();
			
			//$final_data['rows'] = $total_rows;

			$where_clause = array(
				'PROD_ID ='		=>	$prod_id,
				'ISSUE_DT >='	=>	$from_dt_str,
				'ISSUE_DT <='	=>	$to_dt_str
			);

			$or_clause = array(
				'RET_DT >='		=> $from_dt_str,
				'RET_DT <='		=> $to_dt_str		
			);

			//$where_str = "('PROD_ID'='".$prod_id."' AND 'ISSUE_DT' >= '".$from_dt."' AND 'ISSUE_DT' <= '".$to_dt."') OR ";
			//$where_str .= "('PROD_ID'='".$prod_id."' AND 'RET_DT' >='".$from_dt."' AND 'RET_DT' <= '".$to_dt."')";
			$result = 	$this->db->select()->from('ISSUE')
						->join('USER','USER.USER_ID = ISSUE.USER_ID')
						->where($where_clause)->or_where('PROD_ID', $prod_id)
						->where($or_clause)->order_by('ISSUE_DT','desc')->get();
		
			if($result->num_rows() > 0){
				$rows_tot = $result->num_rows();
				foreach($result->result() as $row){
					if(strtotime($row->ISSUE_DT) >= $from_dt && strtotime($row->ISSUE_DT) <= $to_dt){
						$append_data = array(
							'DATE'			=>	$row->ISSUE_DT,
							'USER_NAME'		=>	$row->USER_NAME,
							'TEMP_DT'		=>	strtotime($row->ISSUE_DT),
							'CRDE'			=>	1,
							'QTY'			=>	$row->QTY
						);
						array_push($data,$append_data);
					}

					if($row->RETVAR == 1){
						if(strtotime($row->RET_DT) >= $from_dt && strtotime($row->RET_DT) <= $to_dt){
							$rows_tot++;
							$return_data = array(
								'DATE'		=>	$row->RET_DT,
								'TEMP_DT'	=>	strtotime($row->RET_DT),
								'CRDE'		=>	2,
								'QTY'		=>	$row->QTY,
								'USER_NAME'	=>	$row->USER_NAME
							);
							array_push($data, $return_data);
						}
					}
				}
				$final_data['rows'] = $rows_tot;
			}

			$temp = array();
			foreach($data as $key => $val){
				$temp[$key] = $val['TEMP_DT'];
			}
			$list_data = array();
			$count = 0;
			array_multisort($temp,SORT_DESC,$data);
			
			foreach($data as $key => $val){
				if($count < $from_row)
					$count++;
				else{
					if($count > $to_row)
						$count++;
					else{
						array_push($list_data, $val);
						$count++;
					}
				}
			}
			$final_data['table'] = $list_data;
			return json_encode($final_data);
		}
		else
			return NULL;
	}

	public function user_view(){
		$user_id = $this->input->post('user_id');
		//$user_id = 1;

		if($user_id != NULL){
			$from_dt = $this->input->post('from_dt');
			$to_dt = $this->input->post('to_dt');
			$from_dt = str_replace('/','-',$from_dt);
			$to_dt = str_replace('/','-',$to_dt);
			$from_dt = strtotime($from_dt);
			$to_dt = strtotime($to_dt) + 86400;
			$from_dt_str = date('Y-m-d',$from_dt);
			$to_dt_str = date('Y-m-d',$to_dt);
			$page_no = $this->input->post('pg_no');
			$rows_per_page = 20;
			
			if($page_no == NULL || !is_numeric($page_no))
				$page_no = 1;
			
			//$to_row = $page_no * 20;
			$from_row = (($page_no - 1) * $rows_per_page);
			$to_row = $from_row + $rows_per_page - 1;

			$where_clause = array(
				'USER_ID ='		=>	$user_id,
				'ISSUE_DT >='	=>	$from_dt_str,
				'ISSUE_DT <='	=>	$to_dt_str
			);

			$or_clause = array(
				'RET_DT >='		=> $from_dt_str,
				'RET_DT <='		=> $to_dt_str		
			);

			$result =	$this->db->select()->from('ISSUE')
						->join('PRODUCT','PRODUCT.PROD_ID = ISSUE.PROD_ID')
						->join('CATEGORY','PRODUCT.CAT_ID = CATEGORY.CAT_ID')
						->where($where_clause)->or_where('USER_ID',$user_id)
						->where($or_clause)->order_by('ISSUE_DT','desc')->get();

			$final_data = array();
			$data = array();
			$prod_ids = array();
			//echo $this->db->last_query();
			if($result->num_rows() > 0){
				$rows_tot = $result->num_rows();
				foreach( $result->result() as $row){
					if(strtotime($row->ISSUE_DT) >= $from_dt && strtotime($row->ISSUE_DT) <= $to_dt){
						$append_data = array(
							'DATE'			=>	$row->ISSUE_DT,
							'QTY'			=>	$row->QTY,
							'TEMP_DT'		=>	strtotime($row->ISSUE_DT),
							'PROD_NAME'		=>	$row->PROD_NAME,
							'CAT_NAME'		=>	$row->CAT_NAME,
							'CRDE'			=>	1
						);
						array_push($data,$append_data);
					}

					if($row->RETVAR == 1){
						if(strtotime($row->RET_DT) >= $from_dt && strtotime($row->RET_DT) <= $to_dt){
							$rows_tot++;
							$return_data = array(
								'QTY'				=>	$row->QTY,
								'DATE'				=>	$row->RET_DT,
								'TEMP_DT'			=>	strtotime($row->RET_DT),
								'PROD_NAME'			=>	$row->PROD_NAME,
								'CAT_NAME'			=>	$row->CAT_NAME,
								'CRDE'				=>	2
							);
							array_push($data,$return_data);
						}
					}		
				}
				$final_data['rows'] = $rows_tot;
			}
			$list_data = array();
			$count = 0;
			$temp = array();
			
			foreach($data as $key => $val){
				$temp[$key] = $val['TEMP_DT'];
			}
			array_multisort($temp,SORT_DESC,$data);

			foreach($data as $key => $val){
				if($count < $from_row)
					$count++;
				else{
					if($count > $to_row)
						$count++;
					else{
						array_push($list_data, $val);
						$count++;
					}
				}
			}
			$final_data['table'] = $list_data;
			return json_encode($final_data);
		}
		else
			return NULL;
	}

	public function view_all(){
		$result =  	$this->db->select()->from('PRODUCT')
					->join('CATEGORY','PRODUCT.CAT_ID = CATEGORY.CAT_ID')
					->order_by('AVAIL_QTY','asc')
					->get();

		$return_data = array();

		if($result->num_rows > 0){
			foreach($result->result() as $row){
				array_push($return_data, $row);
			}
			return $return_data;
		}
		else
			return NULL;
	}
}