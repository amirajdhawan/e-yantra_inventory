<?php
class Home extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		//Libraries and Helpers loading
		$this->load->library('session');
		$this->load->helper('loginvalidate');
	}

	public function index($page = 'home'){
		if(!check_login()){
			redirect('account');
		}
		if($this->session->userdata('role') != 1)
			redirect('account/access_denied');

		$data = array(
			'title' 		=> 	ucfirst($page),
			'active_link' 	=> 	7,
			'role'			=> 	$this->session->userdata('role'),
			'mode' 			=>	1
		);
		// print_r($this->session->userdata('role'));
		
		$this->load->model('add_db_content');
		$activity = $this->add_db_content->get_activity();
		$availability = $this->add_db_content->get_avail_issues(); 
		
		if($activity == NULL){
			$data['mode'] =	2;
			$data['availability'] = $availability;
		}
		else{
			$data['activity'] = $activity;
			$data['availability'] = $availability;
			$data['mode'] = 3;
		}
		
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		//print_r( $activity);
		$this->load->view('home/home',$data);
		$this->load->view('template/footer');
	}

	public function add_user(){
		if(!check_login()){
			redirect('account');
		}
		if($this->session->userdata('role') != 1)
			redirect('account/access_denied');

		$data = array(
			'title' => 'Add User',
			'active_link' => 3,
			'role' => $this->session->userdata('role'),
			'mode' => 1);

		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('username', 'User Name', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required|matches[confpassword]|min_length[5]');
		$this->form_validation->set_rules('confpassword', 'Confirm Password', 'required|min_length[5]');
		$this->form_validation->set_rules('type', 'Type', 'required');

		if ($this->form_validation->run() === FALSE)
			$data['mode'] = 1;
		else{
			$this->load->model('add_db_content');
			if($this->add_db_content->add_user_db() === TRUE)
				$data['mode'] = 2;
			else
				$data['mode'] = 3;
		}

		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('home/add_user',$data);
		$this->load->view('template/footer');
	}

	public function add_product(){
		if(!check_login()){
			redirect('account');
		}
		if($this->session->userdata('role') != 1)
			redirect('account/access_denied');

		$data = array(
			'title' => 'Add Product',
			'active_link' => 2,
			'role' => $this->session->userdata('role'),
			'mode' => 1,
			'cat_data' => NULL
		);		
		
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('prod_name', 'Product Name', 'required');
		$this->form_validation->set_rules('prod_totq', 'Total Quantity', 'required|integer');
		$this->form_validation->set_rules('prod_availq', 'Available Quantity', 'required|integer');
		$this->form_validation->set_rules('cat_sel', 'Category', 'required');

		$this->load->model('add_db_content');
		$cat_names = $this->add_db_content->get_cat_data();
		
		if ($this->form_validation->run() === FALSE)
			$data['mode'] = 1;
		
		else{
			$return_data = $this->add_db_content->add_prod_db();
			if($return_data['added'] == 1){
				$data['product_id'] = $return_data['prod_id'];
				$data['mode'] = 2;
			}
			else{
				if($return_data['added'] == 2)
					$data['mode'] = 4;
				else{
					if($return_data['added'] == 3)
						$data['mode'] = 5;
				
					else
						$data['mode'] = 3;
				}
			}
		}

		$data['cat_data'] = $cat_names;

		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('home/add_prod',$data);
		$this->load->view('template/footer');
	}

	public function add_category(){
		if(!check_login()){
			redirect('account');
		}
		if($this->session->userdata('role') != 1)
			redirect('account/access_denied');
		
		$data = array(
			'title' => 'Add Category',
			'active_link' => 1,
			'role' => $this->session->userdata('role'),
			'mode' => 1,
			'cat_id' => FALSE);

		$this->load->library('form_validation');
		$this->form_validation->set_rules('cat_name', 'Category Name', 'required');
		
		if ($this->form_validation->run() === FALSE){
			$data['mode'] = 1;
		}
		else{
			$this->load->model('add_db_content');
			$insert_result = $this->add_db_content->add_cat_db();
		
			if($insert_result['added'] == 1){
				$data['mode'] = 2;
				$data['cat_id'] = $insert_result['cat_id'];
			}
			else{
				if($insert_result['added'] == 2){
					$data['mode'] = 4;
					$data['cat_id'] = $insert_result['cat_id'];
				}
				else
					$data['mode'] = 3;
			}
		}
		
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('home/add_cat',$data);
		$this->load->view('template/footer');
	}

	public function issue(){
		if(!check_login()){
			redirect('account');
		}
		if($this->session->userdata('role') != 1)
			redirect('account/access_denied');
		
		$data = array(
			'title' => 'Issue Hardware',
			'active_link' => 4,
			'role' => $this->session->userdata('role'),
			'mode' => 1
		);

		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('uname', 'User\'s Name', 'required');
		$this->form_validation->set_rules('prod_name', 'Product Name', 'required');
		$this->form_validation->set_rules('quantity', 'Issue Quantity', 'required');
		
		if($this->form_validation->run() === FALSE){
			$data['mode'] = 1;
		}
		else{
			$this->load->model('issue_model');
			$return_val = $this->issue_model->issue_prod();
			if($return_val['mode'] == 1){
				$data['issue_id'] = $return_val['issue_id'];
				$data['mode'] = 2;
			}
			else{
				if($return_val['mode'] == 3)
					$data['mode'] = 4;
				else{
					if($return_val['mode'] == 4)
						$data['mode'] = 5;
					else
						$data['mode'] = 3;
				}
			}		
		}

		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('home/issue',$data);
		$this->load->view('template/footer');
	}

	public function get_names(){
		if(!check_login()){
			redirect('account');
		}
		if($this->session->userdata('role') != 1)
			redirect('account/access_denied');
		
		$this->load->model('issue_model');
		echo $this->issue_model->get_user_names();
	}

	public function get_details(){
		if(!check_login()){
			redirect('account');
		}
		if($this->session->userdata('role') != 1)
			redirect('account/access_denied');
		
		$this->load->model('issue_model');
		echo $this->issue_model->get_user_details();	
	}
	
	public function get_prod_details(){
		if(!check_login()){
			redirect('account');
		}
		if($this->session->userdata('role') != 1)
			redirect('account/access_denied');
		
		$this->load->model('issue_model');
		echo $this->issue_model->get_prod_details();	
	}

	public function get_prods(){
		if(!check_login()){
			redirect('account');
		}
		if($this->session->userdata('role') != 1)
			redirect('account/access_denied');
		
		$this->load->model('issue_model');
		echo $this->issue_model->get_prod_names();
	}

	public function return_hw(){
		if(!check_login()){
			redirect('account');
		}
		if($this->session->userdata('role') != 1)
			redirect('account/access_denied');

		$data = array(
			'title' 		=> 	'Return Hardware',
			'active_link' 	=> 	5,
			'role' 			=> 	$this->session->userdata('role'),
			'mode' 			=> 	1
		);

		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('issue_id', 'Issue ID', 'required');
		$this->form_validation->set_rules('return_quan', 'Return Quantity', 'required');	
		
		if($this->form_validation->run() === FALSE){
			$data['mode'] = 1;
		}
		else{
			$this->load->model('return_model');
			if($this->return_model->return_hardware() == TRUE)
				$data['mode'] = 2;
			else
				$data['mode'] = 3;
		}
		
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('home/return_hw',$data);
		$this->load->view('template/footer');
	}

	public function ajax_get_names(){
		if(!check_login()){
			redirect('account');
		}
		if($this->session->userdata('role') != 1)
			redirect('account/access_denied');

		$this->load->model('return_model');
		echo $this->return_model->get_users_return();
	}

	public function ajax_get_prods(){
		if(!check_login()){
			redirect('account');
		}
		if($this->session->userdata('role') != 1)
			redirect('account/access_denied');

		$this->load->model('return_model');
		echo $this->return_model->get_prods_return();
	}

	public function ajax_get_details(){
		if(!check_login()){
			redirect('account');
		}
		if($this->session->userdata('role') != 1)
			redirect('account/access_denied');

		$this->load->model('return_model');
		echo $this->return_model->get_issue_details();
	}

	public function breakage(){
		if(!check_login()){
			redirect('account');
		}
		if($this->session->userdata('role') != 1)
			redirect('account/access_denied');

		$data = array(
			'title' 		=> 	'Broken/Lost/Gifted',
			'active_link' 	=> 	8,
			'role' 			=> 	$this->session->userdata('role'),
			'mode' 			=> 	5
		);

		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('uname', 'User\'s Name', 'required');
		$this->form_validation->set_rules('prod_name', 'Product Name', 'required');
		$this->form_validation->set_rules('quantity', 'Quantity', 'required');
		
		if($this->form_validation->run() === FALSE){
			$data['mode'] = 5;
		}
		else{
			$this->load->model('issue_model');
			$result = $this->issue_model->add_breakage();
			switch($result['mode']){
				case 1:
					$data['mode'] = 1;
					$data['break_id'] = $result['break_id'];
					break;
				case 2:
					$data['mode'] = 2;
					break;
				case 3:
					$data['mode'] = 3;
					break;
				case 4:
					$data['mode'] = 4;
					break;
				default:
					$data['mode'] = 5;
			}
		}

		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('home/breakage',$data);
		$this->load->view('template/footer');
	}
}