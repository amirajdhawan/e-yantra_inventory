<?php
class View extends CI_Controller {
	
	public function __construct(){
		
		//Constructor
		parent::__construct();
		
		//Libraries and Helpers loading
		$this->load->library('session');
		$this->load->helper('loginvalidate');
	}

	//Index page
	public function index(){
		if(!check_login()){
			redirect('account');
		}

		$data = array(
			'title' 		=> 	'View Statment',
			'active_link' 	=> 	6,
			'role' 			=> 	$this->session->userdata('role'),
			'mode' 			=> 	1,
			'view_link'		=>	1
		);
		$this->load->model('view_model');
		$data['result'] = $this->view_model->view_all();

		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		
		//Base View with the top View control buttons
		//$this->load->view('view/base',$data);
		$this->load->view('view/view_all',$data);
		$this->load->view('template/footer');
	}

	public function category(){
		if(!check_login()){
			redirect('account');
		}
		
		$data = array(
			'title' 		=>	'View Statment - Category',
			'active_link' 	=> 	6,
			'role' 			=>	$this->session->userdata('role'),
			'mode' 			=> 	1,
			'view_link'		=>	2
		);

		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		//$this->load->view('view/base',$data);
		$this->load->view('view/category',$data);
		$this->load->view('template/footer');	
	}

	public function ajax_cat_dets(){
		//Ajax function to get category list dropdown
		if(!check_login()){
			redirect('account');
		}
		$this->load->model('view_model');
		//Call to function get_cat_dets() from model View_Model to get list of categories with their ID's and Name
		echo $this->view_model->get_cat_dets();
	}

	public function get_cat_view(){
		//Get category wise view for View Statment
		if(!check_login()){
			redirect('account');
		}

		$this->load->model('view_model');
		//Call to function cat_view() from model View_Model to get Category based view
		echo $this->view_model->cat_view();
	}

	public function product(){
		if(!check_login()){
			redirect('account');
		}
		$data = array(
			'title' 		=> 	'View Statment - Product',
			'active_link' 	=> 	6,
			'role' 			=> 	$this->session->userdata('role'),
			'mode' 			=> 	1,
			'view_link'		=>	3
		);

		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		//$this->load->view('view/base',$data);
		$this->load->view('view/product',$data);
		$this->load->view('template/footer');		
	}

	public function get_prod_view(){
		//Get Product View
		if(!check_login()){
			redirect('account');
		}
		$this->load->model('view_model');
		echo $this->view_model->prod_view();
	}

	public function ajax_prod_dets(){
		//Ajax function to get category list dropdown
		if(!check_login()){
			redirect('account');
		}
		$this->load->model('view_model');
		//Call to function get_cat_dets() from model View_Model to get list of categories with their ID's and Name
		echo $this->view_model->get_prod_dets();
	}

	public function ajax_user_dets(){
		if(!check_login()){
			redirect('account');
		}
		$this->load->model('issue_model');
		echo $this->issue_model->get_user_names();
	}

	public function get_user_view(){
		if(!check_login()){
			redirect('account');
		}
		$this->load->model('view_model');
		echo $this->view_model->user_view();
	}

	public function user(){
		if(!check_login()){
			redirect('account');
		}
		$data = array(
			'title' 		=> 	'View Statment - User',
			'active_link' 	=> 	6,
			'role' 			=> 	$this->session->userdata('role'),
			'mode' 			=> 	1,
			'view_link'		=>	4
		);
		
		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		//$this->load->view('view/base',$data);
		$this->load->view('view/user',$data);
		$this->load->view('template/footer');	
	}

	public function print_stat($type = ''){
		if(!check_login()){
			redirect('account');
		}

		$data = array(
			'role' 	=> $this->session->userdata('role'),
			'title'	=> ''
		);

		switch($type){
			case 'product':
				$data['title']	=	"Product Statement";
				break;

			case 'user':
				$data['title']	=	"User Statement";
				break;

			case 'all':
				$data['title']	=	"View Statement";
				break;

			case 'category':
				$data['title']	=	"Category Statement";
				break;

			default:
				$data['title']	=	"View Statement";
				break;
		}
		$this->load->view('template/header',$data);
	}
}