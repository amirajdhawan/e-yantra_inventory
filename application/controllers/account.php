<?php
class Account extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->library('session');
		$this->load->helper('loginvalidate');
	}
	
	public function index($page = 'login'){
		$data['title'] = ucfirst($page);

		if(check_login()){
			redirect('home');							//Redirect to home page when done
		}
		
		else{
			//Debug Code
			
			$data['logout'] = 1;
			
			$this->load->view('template/header',$data);
			$this->load->view('account/login');
			$this->load->view('template/footer');
		}
	}

	public function access_denied(){
		if(!check_login()){
			redirect('account');							//Redirect to login if not logged in
		}
		$data = array(
			'title' => 'Access Denied',
			'role' => $this->session->userdata('role'),		
		);

		$this->load->view('template/header',$data);
		$this->load->view('template/sidebar',$data);
		$this->load->view('account/access_denied');
		$this->load->view('template/footer');
	}

	public function process(){
		if(check_login()){
			redirect('home');							//Redirect to home page if already logged in
		}
		$this->load->library('form_validation');

		$this->form_validation->set_rules('username', 'User Name', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		
		if ($this->form_validation->run() === FALSE){
			$this->load->view('template/header');
			$this->load->view('account/login');
			$this->load->view('template/footer');
		}
		
		else{
			$this->load->model('account_model');
			$auth_data = $this->account_model->authenticate();
			//print_r($auth_data);
			if($auth_data['auth'] === 1 && ($auth_data['role'] == 1 || $auth_data['role'] == 2)){
				$data = array(
					'username' => $auth_data['user'],
					'role' => $auth_data['role'],
					'auth' => 1
				);

				$this->session->set_userdata($data);
				$temp_session_var = $this->session->all_userdata();
				
				if($auth_data['role'] == 1)
					redirect('home');
				else
					redirect('view');
			}
			else{
				$this->load->view('login_failed');
			}
		}
	}

	public function logout(){
		if(check_login()){
			$this->session->sess_destroy();
			redirect('account');
		}
		else{
			redirect('account');
		}
	}
}
?>