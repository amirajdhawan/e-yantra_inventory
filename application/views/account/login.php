	<div class = "row-fluid">
		<div class = "span8">
			<h1>Account Login</h1>
			<p><br/><img src="<?= base_url().'assets/img/inventory.jpg'?>" style="width:480px;height:200px;"></p>
		</div>
		<div class = "span4">
				<br/><br/><?php 
					$attributes = array('class' => 'form-signin', 'id' => 'login_form');
					echo form_open('account/process',$attributes); 
				?>
				<h2 class="form-signin-heading">Please log in</h2>
				<input name ="username" type="text" class="input-block-level" placeholder="User Name">
				<input name ="password" type="password" class="input-block-level" placeholder="Password">
				<!--<label class="checkbox">
					<input type="checkbox" value="remember-me"> Remember me
				</label>-->
				<button class="btn btn-large btn-primary" type="submit">Login</button>
				<?php echo validation_errors(); ?>
			</form>
		</div>
	</div>