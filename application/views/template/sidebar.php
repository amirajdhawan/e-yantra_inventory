	<div class="row-fluid">
		<div class="span3">
			<div class="row-fluid">
			<div class="well sidebar-nav">
				<ul class="nav nav-list">
					<?php if($role == 1): ?>
						<li <?php if($active_link == 7 ) echo 'class="active"' ?>><a href="<?= base_url().index_page().'home'?>"><i class="icon-home"></i>Home</a></li>
						<li <?php if($active_link == 1 ) echo 'class="active"' ?>><a href="<?= base_url().index_page().'home/add_category'?>"><i class="icon-plus-sign"></i>Category</a></li>
						<li <?php if($active_link == 2 ) echo 'class="active"' ?>><a href="<?= base_url().index_page().'home/add_product'?>"><i class="icon-tag"></i>Product</a></li>
						<li <?php if($active_link == 3 ) echo 'class="active"' ?>><a href="<?= base_url().index_page().'home/add_user'?>"><i class="icon-user"></i>Add User</a></li>
						<li <?php if($active_link == 4 ) echo 'class="active"' ?>><a href="<?= base_url().index_page().'home/issue'?>"><i class="icon-circle-arrow-left"></i>Issue Hardware</a></li>
						<li <?php if($active_link == 5 ) echo 'class="active"' ?>><a href="<?= base_url().index_page().'home/return_hw'?>"><i class="icon-circle-arrow-right"></i>Return Hardware</a></li>
						<li <?php if($active_link == 8 ) echo 'class="active"' ?>><a href="<?= base_url().index_page().'home/breakage'?>"><i class="icon-remove-sign"></i>Broken/Lost/Gifted</a></li>
						<li <?php if($active_link == 6 ) echo 'class="active"' ?>><a href="<?= base_url().index_page().'view'?>"><i class="icon-list"></i>View Statement</a></li>
					<?php endif ?>
					<?php 
						if($role == 2){
							echo '<li><a href="'.base_url().index_page().'view">View Statement</a></li>';	
						}
					?>
				</ul>
			</div>
			</div>
			<?php if(isset($view_link)):
			?>
			<div class="row-fluid">
				<div class="well sidebar-nav">
					<ul class="nav nav-list">
					<li <?php if($view_link == 1) echo 'class="active"' ?>><a href="<?= base_url().index_page().'view'?>">View All</a></li>
					<li <?php if($view_link == 2) echo 'class="active"' ?>><a href="<?= base_url().index_page().'view/category'?>">Category</a></li>
					<li <?php if($view_link == 3) echo 'class="active"' ?>><a href="<?= base_url().index_page().'view/product'?>">Product</a></li>
					<li <?php if($view_link == 4) echo 'class="active"' ?>><a href="<?= base_url().index_page().'view/user'?>">User</a></li>
					</ul>
				</div>
			</div>
			<?php endif;
			?>
		</div>