<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="<?=base_url();?>assets/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link href="<?=base_url();?>assets/css/bootstrap.min.css" rel="stylesheet" media="print">
	<link href="<?=base_url();?>assets/css/custom.css" rel="stylesheet">
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script> 
	<script src="<?=base_url();?>assets/js/bootstrap.min.js"></script>
	<!--<link href="<?php echo base_url();?>assets/css/bootstrap.css" rel="stylesheet">-->
	<style>
		body {
			padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
		}
		@media print {
		  .visible-print  { display: inherit !important; }
		  .hidden-print   { display: none !important; }
		}
	</style>
	<title><?=$title ?></title>
</head>
<body>
	<div class="navbar navbar-inverse navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">
				<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				</button>
				<a class="brand" href="#">e-Yantra Inventory</a>
				<div class="nav-collapse collapse">
				<ul class="nav">
					<!-- <li class="active"><a href="#">Account</a></li>
					<li><a href="#about">About</a></li>
					<li><a href="#contact">Contact</a></li> -->
					<?php 
						if(!isset($logout)){
							echo '<li><a href="'.base_url().index_page().'account/logout">Logout</a></li>';
						}
					?>
				</ul>
				</div><!--/.nav-collapse -->
			</div>
		</div>
	</div>
	<div class="container">