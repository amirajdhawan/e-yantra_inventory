<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<link href="<?=base_url();?>assets/css/themes/redmond/jquery-ui-1.10.3.custom.min.css" rel="stylesheet">

<div class="span9">
	<br/>
	<div class="row-fluid">
		<div class="span7" style="border:1px solid #e5e5e5;padding:12px;">
		<?php if($mode == 1): ?>
		<div id="dd_div_user" class="input-append btn-group">
		<label>Name:</label><input id="name" name="name" class="input-block-level" size="41" type="text" autocomplete="off">
			<ul id="name_dd" class="dropdown-menu">
			</ul>
		</div>
		<input type="hidden" name="user_id" id="user_id"/>
		<label>From Date: </label><input id="from_dt" class="input-block-level" type="text" readonly autocomplete="off">
		<span style="display:block;height:10px"></span>
		<label>To Date: </label><input id="to_dt" class="input-block-level" type="text" readonly autocomplete="off">
		<?php endif ?>
	</div>
</div>
	<div class="row-fluid">
		<div class="span10">
		<div id="user_view" style="min-width:400px;min-height:300px;"></div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(function() {
		$('#to_dt').datepicker({
			dateFormat: 'dd/mm/yy',
			showOptions: { direction: "down" },
			onSelect: function(){
				var USER_ID = $('#user_id').val();
				get_view(USER_ID,1);
			}
		});
		$('#to_dt').datepicker("setDate", new Date($("#to_dt").val()));
	});
	
	$(function() {
		$('#from_dt').datepicker({
			dateFormat: 'dd/mm/yy',
			showOptions: { direction: "down" },
			onSelect: function(){
				var USER_ID = $('#user_id').val();
				get_view(USER_ID,1);
			}
		});
		var now = new Date();
		if (now.getMonth() == 1) {
		    var current = new Date(now.getFullYear() - 1, 12, now.getDate());
		} else {
		    var current = new Date(now.getFullYear(), now.getMonth() - 1, now.getDate());
		}
		$('#from_dt').datepicker("setDate", current);
	});

function user_click(USER_ID,USER_NAME){
	//alert(USER_ID+' clicked');
	$('#name').val(USER_NAME);
	$('#user_id').val(USER_ID);
	get_view(USER_ID,1);
}

function get_view(USER_ID,PAGE_NO){
	var FROM_DT = $('#from_dt').val();
	var TO_DT = $('#to_dt').val();

	var det_request = $.ajax({
		url: "get_user_view",
		type: "POST",
		data: {user_id : USER_ID,
				pg_no : PAGE_NO,
				from_dt: FROM_DT,
				to_dt: TO_DT},
		dataType: "json"
	});
	
	det_request.done(function(data){
		//alert('in');
		$('#dd_div_user').removeClass('open');
		
		var no_of_rows = data['rows'];
		//alert(no_of_rows);
		var per_page = 20;
		var no_of_pages = Math.ceil(no_of_rows / per_page);
		
		if(PAGE_NO == 1)
			var html_code = '<div class="row-fluid"><div class="pagination"><ul id="page_ul"><li class="disabled"><a>&laquo;</a></li>';
		else
			var html_code = '<div class="row-fluid"><div class="pagination"><ul id="page_ul"><li><a href="#" onclick="javascript:get_view('+USER_ID+','+(PAGE_NO-1)+');">&laquo;</a></li>';

		for(var j = 0; j < no_of_pages; j++){
			if((j+1) == PAGE_NO)
				html_code += '<li class="active"><a onclick="javascript:get_view('+USER_ID+','+(j+1)+');">'+(j+1)+'</a></li>';
			else
				html_code += '<li><a href="#" onclick="javascript:get_view('+USER_ID+','+(j+1)+');">'+(j+1)+'</a></li>';
		}

		if(PAGE_NO == (no_of_pages - 1))
			html_code += '<li><a href="#" onclick="javascript:get_view('+USER_ID+','+(PAGE_NO+1)+');">&raquo;</a></li></ul></div></div>';
		else
			html_code += '<li class="disabled"><a>&raquo;</a></li></ul></div></div>';

		html_code += '<div id="user_table"><table class="table table-hover table-bordered" ><tr><th>#&nbsp;</th><th>Product Name&nbsp;</th><th>Category&nbsp;</th><th>Date&nbsp;</th><th>Credit&nbsp;</th><th>Debit&nbsp;</th></tr>';
		
		for(var i = 0; i < data.table.length; i++){
			//html_code += data[i].CRDE+' ';
			html_code += '<tr><td>'+(i+1)+'</td><td>';
			html_code += data.table[i].PROD_NAME+'</td><td>';
			html_code += data.table[i].CAT_NAME+'</td><td>';
			html_code += data.table[i].DATE+'</td><td>';
			
			if(data.table[i].CRDE == 2){
				html_code += data.table[i].QTY+' CR</td><td>';
			}
			else
				html_code += '</td><td>'+data.table[i].QTY+' DR';
			html_code += '</td></tr>';

		}
		html_code += '</table></div><div style="text-align:center"><button class="btn btn-large btn-primary" onclick="javascript:print_click();">Print</button></div>';
		$('#user_view').html(html_code);
	});
}
</script>
<script type="text/javascript">
$(document).ready(function(){
	$('#name').focus();

	$('#name').keyup(function(event){
		//alert('pressed');
		var user_name = $('#name').val();
		if(user_name == '')
			$('#dd_div_user').removeClass('open');

		if(event.where != 9 && user_name != ''){
			var request = $.ajax({
				url: "ajax_user_dets",
				type: "POST",
				data: {name : user_name},
				dataType: "json"
			});

			request.done(function(data) {
				//alert('return');
				$('#name_dd').html('');
				var flag = 0;
				for(var i = 0; i < data.length; i++){
					flag = 1;
					var append_data = '<li><a href="#" onclick="javascript:user_click('+data[i].USER_ID+',\''+String(data[i].USER_NAME)+'\');">'+data[i].USER_NAME;
					append_data.concat('</a></li>');
					$('#name_dd').append(append_data);
				}
				if(flag == 1)
					$('#name_dd').dropdown('toggle');
				else
					$('#dd_div_user').removeClass('open');
			});
		}
	});
});
</script>
<script type="text/javascript">
	function print_click(){

		var print_content = '';
		var request = $.ajax({
			url: "<?=base_url().index_page()?>view/print_stat/user",
			dataType: "html"
		});

		request.done(function(data) {
			print_content = data;
			print_content += $('#user_table').html();
			
			//$('#temp').html(print_content);
			w = window.open();
			w.document.write(print_content);
			w.print();
		});
	}
</script>