<div class="span9">
	<br/>
	<div style="text-align:left;">
		<?php if($mode == 1): ?>
		<div id="view_all_table">
		<table class="table table-hover table-bordered"><tr><th>#&nbsp;</th><th>Product</th><th>Category</th><th>Available Quantity</th><th>Total Quantity</th><th>Broken/Lost/Gifted</th></tr>
			<?php 
				$count = 1;
				foreach($result as $row){
					echo '<tr><td>'.$count.'</td><td>'.$row->PROD_NAME.'</td><td>'.$row->CAT_NAME.'</td><td>'.$row->AVAIL_QTY.'</td><td>'.$row->TOT_QTY.'</td><td>'.$row->BREAKAGE.'</td></tr>';
					//print_r($row);
					//echo $row->PROD_NAME;
					$count++;
				}
			?>
		</table>
	</div>
	<div style="text-align:center"><button class="btn btn-large btn-primary" onclick="javascript:print_click();">Print</button></div>
		<?php endif; ?>
	</div>
</div>
<script type="text/javascript">
	function print_click(){

		var print_content = '';
		var request = $.ajax({
			url: "<?=base_url().index_page()?>view/print_stat/view_all",
			dataType: "html"
		});

		request.done(function(data) {
			print_content = data;
			print_content += $('#view_all_table').html();
			
			//$('#temp').html(print_content);
			w = window.open();
			w.document.write(print_content);
			w.print();
		});
	}
</script>