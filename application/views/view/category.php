<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<link href="<?=base_url();?>assets/css/themes/redmond/jquery-ui-1.10.3.custom.min.css" rel="stylesheet">

<div class="span9">
	<br/>
	<div class="row-fluid">
		<div class="span7" style="border:1px solid #e5e5e5;padding:12px;">
		<?php if($mode == 1): ?>
		<div id="dd_div_cat" class="input-append btn-group">
		<label>Category Name:</label><input id="cat_name" name="cat_name" class="input-block-level" size="41" type="text" autocomplete="off">
			<ul id="cat_dd" class="dropdown-menu">
			</ul>
		</div>
		<?php endif ?>		
	</div>
</div>
	<div class="row-fluid">
		<div class="span10">
			<div id="cat_view" style="min-width:200px;min-height:300px;"></div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function cat_click(CAT_ID,CAT_NAME){
		$('#cat_name').val(CAT_NAME);
		get_view(CAT_ID,1);
	}
	function get_view(CAT_ID,PAGE_NO){
		var FROM_DT = $('#from_dt').val();
		var TO_DT = $('#to_dt').val();

		var det_request = $.ajax({
			url: "get_cat_view",
			type: "POST",
			data: {cat_id : CAT_ID,
					pg_no : PAGE_NO},
			dataType: "json"
		});
		
		det_request.done(function(data){
			
			$('#dd_div_cat').removeClass('open');
			$('#cat_view').html('');

			var no_of_rows = data['rows'];
			//alert(no_of_rows);
			var per_page = 20;
			var no_of_pages = Math.ceil(no_of_rows / per_page);
			
			if(PAGE_NO == 1)
				var html_code = '<div class="row-fluid"><div class="pagination"><ul id="page_ul"><li class="disabled"><a>&laquo;</a></li>';
			else
				var html_code = '<div class="row-fluid"><div class="pagination"><ul id="page_ul"><li><a href="#" onclick="javascript:get_view('+CAT_ID+','+(PAGE_NO-1)+');">&laquo;</a></li>';

			for(var j = 0; j < no_of_pages; j++){
				if((j+1) == PAGE_NO)
					html_code += '<li class="active"><a onclick="javascript:get_view('+CAT_ID+','+(j+1)+');">'+(j+1)+'</a></li>';
				else
					html_code += '<li><a href="#" onclick="javascript:get_view('+CAT_ID+','+(j+1)+');">'+(j+1)+'</a></li>';
			}

			if(PAGE_NO == (no_of_pages - 1))
				html_code += '<li><a href="#" onclick="javascript:get_view('+CAT_ID+','+(PAGE_NO+1)+');">&raquo;</a></li></ul></div></div>';
			else
				html_code += '<li class="disabled"><a>&raquo;</a></li></ul></div></div>';
			
			html_code += '<div id="cat_table"><table class="table table-hover table-bordered"><tr><th>#&nbsp;</th><th>Product Name&nbsp;</th><th>Available Quantity&nbsp;</th><th>Total Quantity&nbsp;</th><th>Breakage/Lost/Gifted</th></tr>';
			
			for(var i = 0; i < data.table.length; i++){
				html_code += '<tr><td>'+(i+1)+'</td><td>'+data.table[i].PROD_NAME+'</td><td>'+data.table[i].AVAIL_QTY+'</td><td>'+data.table[i].TOT_QTY+'</td><td>'+data.table[i].BREAKAGE+'</td></tr>';
			}
			
			html_code += '</table></div><div style="text-align:center"><button class="btn btn-large btn-primary" onclick="javascript:print_click();">Print</button></div>';
			$('#cat_view').html(html_code);
			$('#dd_div_cat').removeClass('open');
		});	
	}
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#cat_name').focus();

		$('#cat_name').keyup(function(event){
			
			var cat_name = $('#cat_name').val();
			if(cat_name == '')
				$('#dd_div_cat').removeClass('open');

			if(event.which != 9 && cat_name != ''){
		
				var request = $.ajax({
					url: "ajax_cat_dets",
					type: "POST",
					data: {cat_name : cat_name},
					dataType: "json"
				});

				request.done(function(data) {
			
					$('#cat_dd').html('');
					var flag = 0;
					for(var i = 0; i < data.length; i++){
						flag = 1;
						var append_data = '<li><a href="#" onclick="javascript:cat_click('+data[i].CAT_ID+',\''+String(data[i].CAT_NAME)+'\');">'+data[i].CAT_NAME;
						append_data.concat('</a></li>');
						$('#cat_dd').append(append_data);
					}
					if(flag == 1)
						$('#cat_dd').dropdown('toggle');
					else
						$('#dd_div_cat').removeClass('open');
				});
			}
		});
	});
</script>
<script type="text/javascript">
	function print_click(){

		var print_content = '';
		var request = $.ajax({
			url: "<?=base_url().index_page()?>view/print_stat/category",
			dataType: "html"
		});

		request.done(function(data) {
			print_content = data;
			print_content += $('#cat_table').html();
			
			//$('#temp').html(print_content);
			w = window.open();
			w.document.write(print_content);
			w.print();
		});
	}
</script>