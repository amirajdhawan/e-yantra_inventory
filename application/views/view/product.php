<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<link href="<?=base_url();?>assets/css/themes/redmond/jquery-ui-1.10.3.custom.min.css" rel="stylesheet">

<div class="span9">
		<br/>
		<?php if($mode == 1): ?>
		
		<div class="row-fluid" style="border:1px solid #e5e5e5;padding:12px;">
		<div class="span6" style="margin-left:0px;">
			<!-- <label>Product ID:</label><input id="prod_id" name="prod_id" class="input-block-level" readonly size="41" type="text" autocomplete="off"> -->
			<div id="dd_div_prod" class="input-append btn-group">
			<label>Product Name:</label><input id="prod_name" name="prod_name" class="input-block-level" size="41" type="text" autocomplete="off">
				<ul id="prod_dd" class="dropdown-menu">
				</ul>
			</div>
			<input type="hidden" name = "prod_id" id="prod_id"/>
			<label>Product Description:</label><textarea id="prod_desc" name="prod_desc" readonly class="input-block-level" size="41" type="text" autocomplete="off"></textarea>
			<label>Total Quantity:</label><input id="tot_qty" name="tot_qty" class="input-block-level" readonly size="41" type="text" autocomplete="off">
		</div>
		<div class="span6">
			<label>Available Quantity:</label><input id="avail_qty" name="avail_qty" class="input-block-level" readonly size="41" type="text" autocomplete="off">
			<span style="display:block;height:9px"></span>
			<label>From Date: </label><input id="from_dt" class="input-block-level" type="text" readonly autocomplete="off">
			<span style="display:block;height:10px"></span>
			<label>To Date: </label><input id="to_dt" class="input-block-level" type="text" readonly autocomplete="off">
		</div>
	</div>
		<?php endif ?>
	<div id="prod_view" class="visible-print" style="min-width:400px;min-height:300px;">
	</div>
</div>
<script type="text/javascript">
	$(function() {
		$('#to_dt').datepicker({
			dateFormat: 'dd/mm/yy',
			showOptions: { direction: "down" },
			onSelect: function(){
				var PROD_ID = $('#prod_id').val();
				get_view(PROD_ID,1);
			}
		});
		$('#to_dt').datepicker("setDate", new Date($("#to_dt").val()));
	});
	
	$(function() {
		$('#from_dt').datepicker({
			dateFormat: 'dd/mm/yy',
			showOptions: { direction: "down" },
			onSelect: function(){
				var PROD_ID = $('#prod_id').val();
				get_view(PROD_ID,1);
			}
		});
		var now = new Date();
		if (now.getMonth() == 1) {
		    var current = new Date(now.getFullYear() - 1, 12, now.getDate());
		} else {
		    var current = new Date(now.getFullYear(), now.getMonth() - 1, now.getDate());
		}
		$('#from_dt').datepicker("setDate", current);
	});

	function prod_click(PROD_ID,PROD_NAME, DESCRIPTION, TOT_QTY, AVAIL_QTY){
		//alert('wow');
		$('#prod_id').val(PROD_ID);
		$('#prod_name').val(PROD_NAME);
		$('#prod_desc').val(DESCRIPTION);
		$('#tot_qty').val(TOT_QTY);
		$('#avail_qty').val(AVAIL_QTY);
		get_view(PROD_ID,1);
	}

	function get_view(PROD_ID, PAGE_NO){
		var FROM_DT = $('#from_dt').val();
		var TO_DT = $('#to_dt').val();
		//alert(FROM_DT);
		var det_request = $.ajax({
			url: "get_prod_view",
			type: "POST",
			data: {	prod_id : PROD_ID,
					from_dt : FROM_DT,
					to_dt   : TO_DT,
					pg_no 	: PAGE_NO},
			dataType: "json"
		});
		
		det_request.done(function(data){
			//alert('in');
			var no_of_rows = data['rows'];
			//alert(no_of_rows);
			var per_page = 20;
			var no_of_pages = Math.ceil(no_of_rows / per_page);
			
			if(PAGE_NO == 1)
				var html_code = '<div class="row-fluid"><div class="pagination"><ul id="page_ul"><li class="disabled"><a>&laquo;</a></li>';
			else
				var html_code = '<div class="row-fluid"><div class="pagination"><ul id="page_ul"><li><a href="#" onclick="javascript:get_view('+PROD_ID+','+(PAGE_NO-1)+');">&laquo;</a></li>';

			for(var j = 0; j < no_of_pages; j++){
				if((j+1) == PAGE_NO)
					html_code += '<li class="active"><a onclick="javascript:get_view('+PROD_ID+','+(j+1)+');">'+(j+1)+'</a></li>';
				else
					html_code += '<li><a href="#" onclick="javascript:get_view('+PROD_ID+','+(j+1)+');">'+(j+1)+'</a></li>';
			}

			if(PAGE_NO == (no_of_pages - 1))
				html_code += '<li><a href="#" onclick="javascript:get_view('+PROD_ID+','+(PAGE_NO+1)+');">&raquo;</a></li></ul></div></div>';
			else
				html_code += '<li class="disabled"><a>&raquo;</a></li></ul></div></div>';
			
			html_code += '<div id="prod_table"><table border="1" class="table table-hover table-bordered"><tr><th>#&nbsp;</th><th>User Name&nbsp;</th><th>Date&nbsp;</th><th>Credit&nbsp;</th><th>Debit&nbsp;</th></tr>';
			//alert(data.table[0].USER_NAME);
			for(var i = 0; i < data.table.length; i++){
				//html_code += data[i].CRDE+' ';
				html_code += '<tr><td>'+(i+1)+'</td><td>';
				html_code += data.table[i].USER_NAME+'</td><td>';
				html_code += data.table[i].DATE+'</td><td>';

				if(data.table[i].CRDE == 2){
					html_code += data.table[i].QTY+' CR</td><td>';
				}
				else{
					html_code += '</td><td>'+data.table[i].QTY+' DR';
				}
				html_code += '</td></tr>';

			}
			html_code += '</table></div>';
			html_code += '<div style="text-align:center"><button class="btn btn-large btn-primary" onclick="javascript:print_click();">Print</button></div>';
			$('#prod_view').html('');
			$('#prod_view').html(html_code);
				
			$('#dd_div_prod').removeClass('open');

		});
	}
</script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#prod_name').focus();

		$('#prod_name').keyup(function(event){
			//alert('pressed');
			var prod_name = $('#prod_name').val();
			
			if(prod_name == '')
				$('#dd_div_prod').removeClass('open');

			if(event.where != 9 && prod_name != ''){
				var request = $.ajax({
					url: "ajax_prod_dets",
					type: "POST",
					data: {prod_name : prod_name},
					dataType: "json"
				});

				request.done(function(data) {
					//alert('return');
					$('#prod_dd').html('');
					var flag = 0;
					for(var i = 0; i < data.length; i++){
						flag = 1;
						var append_data = '<li><a href="#" onclick="javascript:prod_click('+data[i].PROD_ID+',\''+String(data[i].PROD_NAME)+'\',\''+String(data[i].DESCRIPTION)+'\','+data[i].TOT_QTY+','+data[i].AVAIL_QTY+');">'+data[i].PROD_NAME;
						append_data.concat('</a></li>');
						$('#prod_dd').append(append_data);
					}
					if(flag == 1)
						$('#prod_dd').dropdown('toggle');
					else
						$('#dd_div_prod').removeClass('open');
				});
			}
		});
	});
</script>
<script type="text/javascript">
	function print_click(){

		var print_content = '';
		var request = $.ajax({
			url: "<?=base_url().index_page()?>view/print_stat/product",
			dataType: "html"
		});

		request.done(function(data) {
			print_content = data;
			print_content += $('#prod_table').html();
			
			//$('#temp').html(print_content);
			w = window.open();
			w.document.write(print_content);
			w.print();
			//print();
		});
	}
</script>