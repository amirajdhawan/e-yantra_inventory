<div class="span9">
		<?php 
			$attributes = array('class' => 'form-signin', 'id' => 'login_form');
			echo form_open('home/add_category',$attributes);		
		?>
			<h2 class="form-signin-heading">Enter Category details</h2>
			<?php 
			if(validation_errors() != FALSE){
				echo '<div class="row-fluid"><span class="label label-important">'.validation_errors().'</span></div><br/>';
			}
			if($mode == 3){
				echo '<div class="row-fluid"><span class="label label-important" style="font-size:15px;">Unable to add new category. Please retry</span></div><br/>';
			}
			if($mode == 2){
				echo '<div class="row-fluid"><span class="label label-success" style="font-size:15px;">Category added Successfully with ID: '.$cat_id.'</span></div><br/>';
			}
			if($mode == 4){
				echo '<div class="row-fluid"><span class="label label-info" style="font-size:15px;">Category Already exists! with ID: '.$cat_id.'</span></div><br/>';	
			}
			?>
			<div class="row-fluid">
			<label>Category Name:<span style="color:red;">&nbsp;&#42;</span></label><input name ="cat_name" type="text" class="input-block-level">
			<div style="text-align:right;"><button class="btn btn-large btn-primary" type="submit">Add Category</button></div>
		</div>
		</form>
	</div>