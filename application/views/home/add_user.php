	<div class="span9">
		<?php 
			$attributes = array('class' => 'form-signin', 'id' => 'login_form');
			echo form_open('home/add_user',$attributes);		
		?>
			<div class="row-fluid">
				<h2 class="form-signin-heading">Enter Details for the new user</h2>
			</div>
			<div class="row-fluid">
				<?php
					if(validation_errors() != FALSE)
						echo '<div class="row-fluid"><span class="label label-important">'.validation_errors().'</span></div><br/>';

					if($mode == 3)
						echo '<div class="row-fluid"><span class="label label-important">Unable to add new user. Please retry</span></div><br/>';

					if($mode == 2)
						echo '<div class="row-fluid"><span class="label label-info">User added Successfully!</span></div><br/>';
					
				?>
				<div class="row-fluid">
				<div class="span10">
					<label>Username:<span style="color:red;">&nbsp;&#42;</span></label><input name ="username" type="text" class="input-block-level">
					<label>Password:<span style="color:red;">&nbsp;&#42;</span></label><input name ="password" type="password" class="input-block-level">
					<label>Confirm Password:<span style="color:red;">&nbsp;&#42;</span></label><input name ="confpassword" type="password" class="input-block-level"><br/>
					<input type="radio" name="type" value="admin"><span class="50pxspace" >&nbsp;</span>Administrator<br/>
					<input type="radio" name="type" value="viewer"><span class="50pxspace" >&nbsp;</span>Viewer<br/><br/>
				</div>
				</div>
			</div>
			<div class="row-fluid" style="padding:20px;">
				<button class="btn btn-large btn-primary" type="submit">Add User</button>
			</div>
		</form>
	</div>