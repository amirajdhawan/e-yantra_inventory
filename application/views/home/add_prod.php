	<div class = "span9">
	<?php 
			if($mode == 3){
				echo "Unable to add new product. Please retry";
			}
			$attributes = array('id' => 'login_form');
			echo form_open('home/add_product',$attributes);		
	?>
			<div class="row-fluid">
				<h2 class="form-signin-heading">Enter Product details</h2>
			</div>
			<br/>
			<div class="row-fluid" style="border:1px solid #e5e5e5;padding:12px;">
				<?php
					if(validation_errors() != FALSE){
						echo '<div class="row-fluid"><span class="label label-important">'.validation_errors().'</span></div><br/>';
					}
					if($mode == 2){
						echo '<div class="row-fluid"><span class="label label-success" style="font-size:15px;">Successfully added product with ID: '.$product_id.'</span></div></br>';
					}
					if($mode == 4){
						echo '<div class="row-fluid"><span class="label label-info" style="font-size:15px;">Such a product already exits!</span></div><br/>';
					}
					if($mode == 5){
						echo '<div class="row-fluid"><span class="label label-important">Available quantity cannot be greater than total quantity</span></div><br/>';
					}
				?>
				<div class="row-fluid">
				<div class="span6">
					<div style="width:98%;min-height:85px"><label style="float:left;">Category:<span style="color:red;">&nbsp;&#42;</span></label><br/><br/>
						<select style="float:left;" name="cat_sel">
							<?php
								foreach($cat_data as $cat_id => $cat_name) {
									echo'<option value="' . $cat_id . '">' . $cat_name . '</option>';
								}
							?>
						</select>
					</div>
					<label>Product Name:<span style="color:red;">&nbsp;&#42;</span></label><input name ="prod_name" type="text" class="input-block-level">
					<label>Product Manufacturer:</label><input name ="prod_man" type="text" class="input-block-level">
				</div>
				<div class="span6">
					<label>Product Description:</label><textarea name ="prod_desc" class="input-block-level" rows="2"></textarea>			
					<label>Total Quantity:<span style="color:red;">&nbsp;&#42;</span></label><input name ="prod_totq" type="text" class="input-block-level">
					<label>Available Quantity:<span style="color:red;">&nbsp;&#42;</span></label><input name ="prod_availq" type="text" class="input-block-level">
				</div>
			</div>
			</div>
			<div class="row-fluid" style="padding:20px;text-align:right;">
				<button class="btn btn-large btn-primary" type="submit">Add Product</button>
			</div>
		</form>
	<div class="row-fluid">
		<div class="span12">
			<?php 
					// if($mode == 3){
					// 	echo "Unable to add new product. Please retry";
					// }
					$attributes = array('id' => 'login_form');
					echo form_open('home/update_product',$attributes);		
			?>
					<div class="row-fluid">
						<h2 class="form-signin-heading">Update Product</h2>
					</div>
					<br/>
					<div class="row-fluid" style="border:1px solid #e5e5e5;padding:12px;">
						<div class="row-fluid">
						<div class="span6">
							<div id="dd_div_prod" class="input-append btn-group">
								<label>Product Name:<span style="color:red;">&nbsp;&#42;</span></label><input id = "prod_nameu" name="prod_nameu" class="input-block-level" size="41" type="text" autocomplete="off">
								<ul id="dd_prod" class="dropdown-menu">
								</ul>
							</div>
							<label>Product Manufacturer:</label><input id="prod_manuu" name ="prod_manuu" type="text" readonly class="input-block-level">
							<label>Product Description:</label><textarea id ="prod_descu" name ="prod_descu" readonly class="input-block-level" rows="2"></textarea>
						</div>
						<div class="span6">
							<label>Total Quantity:<span style="color:red;">&nbsp;&#42;</span></label><input id="prod_totqu" name ="prod_totqu" type="text" readonly class="input-block-level">
							<label>Available Quantity:<span style="color:red;">&nbsp;&#42;</span></label><input id="prod_availqu" name ="prod_availqu" type="text" readonly class="input-block-level">
							<br/><br/>
							<div class="row-fluid" style="text-align:right;">
								<button class="btn btn-large btn-primary" type="submit">Update Product</button>
							</div>
						</div>
						</div>
					</form>
					</div>
		</div>
	</div>
	</div>
	<script type="text/javascript">

		$(document).ready(function(){
			
			//$('.dropdown-menu').dropdown();

			$('#prod_nameu').keyup(function(event){
				var prod_name = $('#prod_nameu').val();
				if(prod_name == '')
					$('#dd_div_prod').removeClass('open');

				if(event.which != 9 && prod_name !=''){
					
					var request = $.ajax({
						url: "get_prods",
						type: "POST",
						data: {name : prod_name},
						dataType: "json"
					});

					request.done(function(data) {

						$('#dd_prod').html('');
						//$('.dropdown-menu').dropdown('toggle');
						var flag = 0;
						for(var i = 0; i < data.length; i++){
							//var row = data[i];
							flag = 1;
							var append_data = '<li><a href="#" onclick="javascript:prod_click('+data[i].PROD_ID+')">'+data[i].PROD_NAME;
							//$('.testing_temp').html(data[i].USER_NAME);
							append_data.concat('</a></li>');
							$('#dd_prod').append(append_data);
						}
						if(flag == 1)
							$('#dd_prod').dropdown('toggle');
						else
							$('#dd_div_prod').removeClass('open');
					});
				}
			});
		});
	
	function prod_click(PROD_ID){
		//alert(PROD_ID+' clicked');
		var det_request = $.ajax({
			url: "get_prod_details",
			type: "POST",
			data: {prod_id : PROD_ID},
			dataType: "json"
		});

		det_request.done(function(data){
			//alert("in");
			$('#prod_nameu').val(data.PROD_NAME);	
			$('#prod_manuu').val(data.MANUFACT);
			$('#prod_availqu').val('Available Quantity: ' + data.AVAIL_QTY);
			$('#prod_totqu').val(data.TOT_QTY);
			$('#prod_descu').val(data.DESCRIPTION);
			$('#dd_div_prod').removeClass('open');
		});
	}
	</script>
