<style>
	.input-append .btn.dropdown-toggle {
			float: none;
	}
</style>
	<div class="span9">
		<?php 
			//if($mode == 1 || $mode == 3 || $mode == 4 || $mode == 5 ):
			$attributes = array('id' => 'login_form');
			echo form_open('home/issue',$attributes);		
		?>
			<div class="row-fluid">
				<h2 class="form-signin-heading">Enter Issue details</h2>
			</div><br/>
			<div class="row-fluid" style="border: 1px solid #e5e5e5;padding:12px;">
					<?
						if(validation_errors() != FALSE){
							echo '<div class="row-fluid"><span class="label label-important">'.validation_errors().'</span></div><br/>';
						}
						if($mode == 3){
							echo '<div class="row-fluid"><span class="label label-important">Unable to add new Issue. Please retry</span></div><br/>';
						}
						if($mode == 4){
							echo '<div class="row-fluid"><span class="label label-important">Quantity cannot be more than Available Quantity</span></div><br/>';
						}
						if($mode == 5){
							echo '<div class="row-fluid"><span class="label label-important">Issue Quantity cannot be less than 1</span></div><br/>';
						}
						if($mode == 2){
							echo '<div class="row-fluid"><span class="label label-success" style="font-size:15px;">Product successfully issued with ID: '.$issue_id.'</span></div><br/>';
							//echo '<div class="row-fluid"><span class="label label_success">Product successfully Issued</span></div><br/>';
						}		
					?>
			<div class="row-fluid">
				<div class="span6">
					<div id="dd_div_user" class="input-append btn-group">
						<label>Name:<span style="color:red;">&nbsp;&#42;</span></label><input id="uname" name="uname" class="input-block-level" size="41" type="text"  autocomplete="off">
						<ul id="dd_user" class="dropdown-menu"></ul>
					</div>
					<label>Address:</label><input id="uaddress" name ="uaddress" type="text" class="input-block-level" autocomplete="off">
					<label>Email:</label><input id="uemail" name ="uemail" type="text" class="input-block-level" autocomplete="off">
					<label>Phone Number:</label><input id="uphn" name ="uphn" type="text" class="input-block-level" autocomplete="off">
					<label>Issue Quantity:<span style="color:red;">&nbsp;&#42;</span></label><input id = "quantity" name="quantity" class="input-block-level" type="text" autocomplete="off">
				</div>
				<div class="span6">
					<div id="dd_div_prod" class="input-append btn-group">
						<label>Product Name:<span style="color:red;">&nbsp;&#42;</span></label><input id = "prod_name" name="prod_name" class="input-block-level" size="41" type="text" autocomplete="off">
						<ul id="dd_prod" class="dropdown-menu">
						</ul>
					</div>
					<label>Product Manufacturer:</label><input id = "prod_manu" name="prod_manu" class="input-block-level" readonly type="text" autocomplete="off">
					<label>Product Description:</label><textarea id ="prod_desc" name ="prod_desc" class="input-block-level" rows="4" readonly autocomplete="off"></textarea>
					<span style="height:5px;display:block;"></span>
					<label>Available Quantity:<span style="color:red;">&nbsp;&#42;</span></label><input id = "avail_quantity" name="avail_quantity" class="input-block-level" readonly type="text" autocomplete="off">
				</div>
			</div>
			</div>
			<div style="text-align:right;padding:20px;">
				<button class="btn btn-large btn-primary" type="submit">Issue</button>
			</div>
		</form>
	</div>
	
	<script type="text/javascript">
		$(document).ready(function(){
			
			// $('#uname').focusout(function(){
			// 	$('#dd_div_user').removeClass('open');
			// });

			// $('#prod_name').focusout(function(){
			// 	$('#dd_div_prod').removeClass('open');
			// });

			$('#uname').keyup(function(event){
				//alert(event.shiftKey);
				username = $('#uname').val();
				if(username == '')
					$('#dd_div_user').removeClass('open');

				if(event.which != 9 && username != ''){
			
					var request = $.ajax({
						url: "get_names",
						type: "POST",
						data: {name : username},
						dataType: "json"
					});

					request.done(function(data) {
						$('#dd_user').html('');
						//$('.dropdown-menu').dropdown('toggle');
						var flag = 0;
						for(var i = 0; i < data.length; i++){
							//var row = data[i];
							flag = 1;
							var append_data = '<li><a href="#" onclick="javascript:name_click('+data[i].USER_ID+')">'+data[i].USER_NAME;
							//$('.testing_temp').html(data[i].USER_NAME);
							append_data.concat('</a></li>');
							$('#dd_user').append(append_data);
						}
						if(flag == 1)
							$('#dd_user').dropdown('toggle');
						else
							$('#dd_div_user').removeClass('open');
					});
				}
			});
		});
	</script>

	<script type="text/javascript">
		$(document).ready(function(){
			$('#uname').focus();
		});
		$(document).ready(function(){
			
			//$('.dropdown-menu').dropdown();

			$('#prod_name').keyup(function(event){
				var prod_name = $('#prod_name').val();
				if(prod_name == '')
					$('#dd_div_prod').removeClass('open');

				if(event.which != 9 && prod_name !=''){
					
					var request = $.ajax({
						url: "get_prods",
						type: "POST",
						data: {name : prod_name},
						dataType: "json"
					});

					request.done(function(data) {

						$('#dd_prod').html('');
						//$('.dropdown-menu').dropdown('toggle');
						var flag = 0;
						for(var i = 0; i < data.length; i++){
							//var row = data[i];
							flag = 1;
							var append_data = '<li><a href="#" onclick="javascript:prod_click('+data[i].PROD_ID+')">'+data[i].PROD_NAME;
							//$('.testing_temp').html(data[i].USER_NAME);
							append_data.concat('</a></li>');
							$('#dd_prod').append(append_data);
						}
						if(flag == 1)
							$('#dd_prod').dropdown('toggle');
						else
							$('#dd_div_prod').removeClass('open');
					});
				}
			});
		});
	</script>
	<script type="text/javascript">
		function name_click(USER_ID){
			//alert(USER_ID + ' click');
			var det_request = $.ajax({
				url: "get_details",
				type: "POST",
				data: {user_id : USER_ID},
				dataType: "json"
			});

			det_request.done(function(data){
				//alert("inside");
				$('#uname').val(data.USER_NAME);
				$('#uphn').val(data.PHONE_NUM);
				$('#uemail').val(data.EMAIL);
				$('#uaddress').val(data.ADDRESS);
				$('#dd_div_user').removeClass('open');
				//$('#dd_div_user').removeClass('open');
				
			});
		}

		function prod_click(PROD_ID){
			//alert(USER_ID+' clicked');
			var det_request = $.ajax({
				url: "get_prod_details",
				type: "POST",
				data: {prod_id : PROD_ID},
				dataType: "json"
			});

			det_request.done(function(data){
				$('#prod_name').val(data.PROD_NAME);	
				$('#prod_manu').val(data.MANUFACT);
				$('#avail_quantity').val('Available Quantity: ' + data.AVAIL_QTY);
				$('#prod_desc').val(data.DESCRIPTION);
				$('#dd_div_prod').removeClass('open');
			});
		}
	</script>