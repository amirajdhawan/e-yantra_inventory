	<div class = "span9">
		<h2>Notifications</h2><br/>
		<div class="row-fluid">
			<div class="span5" style="border:1px solid #e5e5e5;padding:15px;font-size:25px;">
				<h4>Availability Issues</h4>
				<?php
					foreach($availability as $row){
						if($row->TEMP == 1)
							echo '<span class="label label-important" style="font-size:15px;">Quantity of '.ucfirst($row->PROD_NAME).' is very low!</span><br/>';
					}
					foreach ($availability as $row) {
						if($row->TEMP == 2)
							echo '<span class="label label-warning" style="font-size:15px;">Quantity of '.ucfirst($row->PROD_NAME).' is low!</span><br/>';
					}
				?>
			</div>
			<div class="span7" style="border:1px solid #e5e5e5;padding:15px;">
				<!-- <h4>Activity (Last 10-20 Issue or returns)</h4> -->
				<h4>Today's Activity</h4>
				<?php 
					if($mode == 2){
						echo '<span class="label label-important" style="font-size:15px;">No Activity Today!</span>';
					}
				if($mode == 3): 
				?>
				<table class="table table-hover table-bordered">
					<tr><th>#</th><th>Name</th><th>Product Name</th><th>Date</th><th>Quantity</th></tr>
					<?php
						$count = 1;
						foreach($activity as $row){
							$tab_row = '<tr><td>'.$count.'</td><td>'.$row->USER_NAME.'</td>';
							$tab_row .= '<td>'.$row->PROD_NAME.'</td><td>';
							if($row->RET_DT != NULL)
								$tab_row .= $row->RET_DT;
							else	
								$tab_row .= $row->ISSUE_DT;
							
							$tab_row .= '</td><td>'.$row->QTY.'</td></tr>';
							$count++;
							echo $tab_row;
						}
					?>
				</table>
				<?php endif;
				?>
			</div>
		</div>
	</div>