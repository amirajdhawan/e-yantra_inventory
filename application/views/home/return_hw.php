<style>
	.input-append .btn.dropdown-toggle {
			float: none;
	}
</style>
<div class="span9">
	<?php 
		$attributes = array('id' => 'login_form');
		echo form_open('home/return_hw',$attributes);		
	?>
	<div class="row-fluid">
		<h2 class="form-signin-heading">Enter Return details</h2>
	</div>
	<br/>
	<div class="row-fluid" style="border:1px solid #e5e5e5;padding:12px;">
		<?php
			if(validation_errors() != FALSE){
				echo '<div class="row-fluid"><span class="label label-important">'.validation_errors().'</span></div><br/>';
			}
			if($mode == 3){
				echo '<div class="row-fluid"><span class="label label-important">Return Quantity cannot be more than Issued Quantity</span></div><br/>';
			}
			if($mode == 2){
				echo '<div class="row-fluid"><span class="label label-success" style="font-size:15px;">Product Returned Successfully</span></div><br/>';
			}
		?>
		<div class="row-fluid">
		<div class="span6">
			<div id="dd_div_user" class="input-append btn-group">
				<label>Name:<span style="color:red;">&nbsp;&#42;</span></label><input id="uname" name="uname" class="input-block-level" size="41" type="text" autocomplete="off">
					<ul id="dd_user" class="dropdown-menu">
					</ul>
			</div>
			<input id="issue_id" name="issue_id" type="hidden">
			<label>Product Description:</label><input id="prod_desc" name="prod_desc" class="input-block-level" size="41" readonly type="text" autocomplete="off">
			<label>Issue Date:</label><input id="issue_date" name="issue_date" class="input-block-level" size="41" readonly type="text" autocomplete="off">
		</div>
		<div class="span6">
			<div id="dd_div_prod" class="input-append btn-group">
				<label>Product Name:<span style="color:red;">&nbsp;&#42;</span></label><input id="prod_name" name="prod_name" class="input-block-level" size="41" type="text" autocomplete="off">
					<ul id="dd_prod" class="dropdown-menu">
					</ul>
			</div>
			<label>Issued Quantity:</label><input id="quantity" name="quantity" class="input-block-level" size="41" readonly type="text" autocomplete="off">
			<label>Return Quantity:<span style="color:red;">&nbsp;&#42;</span></label><input id="return_quan" name="return_quan" class="input-block-level" size="41" type="text" autocomplete="off">
		</div>
	</div>
	</div>
	<div class="row-fluid" style="text-align:right;padding:20px;">
		<button class="btn btn-large btn-primary" type="submit">Return</button>
	</div>
</div>
</form>

<script type="text/javascript">
	$(document).ready(function(){		
		$('#uname').keyup(function(event){
			var username = $('#uname').val();
			//alert(username);
			if(username == '')
				$('#dd_div_user').removeClass('open');

			if((event.which != 9) && username != ''){
				var request = $.ajax({
					url: "ajax_get_names",
					type: "POST",
					data: {uname : username},
					dataType: "json"
				});

				request.done(function(data) {
					$('#dd_user').html('');
					var flag = 0;
					for(var i = 0; i < data.length; i++){
						flag = 1;
						var append_data = '<li><a href="#" onclick="javascript:name_click('+data[i].USER_ID+','+data[i].PROD_ID+')">'+data[i].USER_NAME+' - '+data[i].PROD_NAME+' - '+data[i].ISSUE_DT+' - '+data[i].QTY;
						append_data.concat('</a></li>');
						$('#dd_user').append(append_data);
					}
					if(flag == 1)
						$('#dd_user').dropdown('toggle');
					else
						$('#dd_div_user').removeClass('open');
				});
			}
		});
	});
</script>
<script type="text/javascript">
		$(document).ready(function(){
			$("#uname").focus();	
		});

		$(document).ready(function(){
			
			$('#prod_name').keyup(function(event){
				var prod_name = $('#prod_name').val();
				//alert('Prod Ajax');
				if(prod_name == '')
					$("#dd_div_prod").removeClass('open');

				if((event.which != 9) && prod_name != '') {
					var request = $.ajax({
						url: "ajax_get_prods",
						type: "POST",
						data: {pname : prod_name},
						dataType: "json"
					});

					request.done(function(data) {

						$('#dd_prod').html('');
						//alert('result');
						var flag = 0;
						for(var i = 0; i < data.length; i++){
							flag = 1;
							var append_data = '<li><a href="#" onclick="javascript:name_click('+data[i].USER_ID+','+data[i].PROD_ID+')">'+data[i].USER_NAME+' - '+data[i].PROD_NAME;
							append_data.concat('</a></li>');
							$('#dd_prod').append(append_data);
						}
						if(flag == 1)
							$('#dd_prod').dropdown('toggle');
						else
							$('#dd_div_prod').removeClass('open');
					});
				}
			});
		});
	</script>
	<script type="text/javascript">
		function name_click(USER_ID, PROD_ID){
			//alert('HI');
			var det_request = $.ajax({
				url: "ajax_get_details",
				type: "POST",
				data: {	user_id : USER_ID,
						prod_id : PROD_ID},
				dataType: "json"
			});

			det_request.done(function(data){
				//alert(data.ISSUE_DETAILS[0].ISSUE_ID);
				$('#uname').val(data.ISSUE_DETAILS[0].USER_NAME);
				$('#prod_desc').val(data.PROD_DETAIL[0].DESCRIPTION);
				$('#issue_date').val(data.ISSUE_DETAILS[0].ISSUE_DT);
				$('#quantity').val(data.ISSUE_DETAILS[0].QTY);
				$('#prod_name').val(data.PROD_DETAIL[0].PROD_NAME);
				$('#issue_id').val(data.ISSUE_DETAILS[0].ISSUE_ID);
				$('#return_quan').val(data.ISSUE_DETAILS[0].QTY);
				
				// $('#dd_div_user').removeClass('open');
				$('#dd_div_user').removeClass('open');
				$('#dd_div_prod').removeClass('open');
				
			});
		}
	</script>